﻿/*
 * 5. Write a program which creates two threads and a shared collection:
 * the first one should add 10 elements into the collection and the second should print all elements
 * in the collection after each adding.
 * Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.
 */
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    class Program
    {
        static List<int> List = new List<int>();

        static Semaphore Semaphore = new Semaphore(1, 1);

        static Random Random = new Random();

        const int ElementsCount = 10;
        static void Main(string[] args)
        {
            Console.WriteLine("5. Write a program which creates two threads and a shared collection:");
            Console.WriteLine("the first one should add 10 elements into the collection and the second should print all elements in the collection after each adding.");
            Console.WriteLine("Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.");
            Console.WriteLine();

            
            Task.Factory.StartNew(() => AddElementsIntoArray(ElementsCount));
            Task.Factory.StartNew(() => PrintListElementsMultipleTimes(ElementsCount));

            Console.ReadLine();
        }

        static void AddElementsIntoArray(int count)
        {

            for (int i = 0; i < count; i++)
            {
                DoSomethingWithList(ActionType.AddElement);
            }
        }

        static void PrintListElementsMultipleTimes(int times)
        {
            for (int i = 0; i < times; i++)
            {
                DoSomethingWithList(ActionType.PrintElements);
            }
        }

        static void DoSomethingWithList(ActionType actionType)
        {
            Semaphore.WaitOne();
            switch (actionType)
            {
                case ActionType.AddElement:
                    List.Add(Random.Next(20));
                    break;
                case ActionType.PrintElements:
                    PrintListElements();
                    break;

            }
            Semaphore.Release();
        }

        static void PrintListElements()
        {
            Console.Write("[");
            for (int i = 0; i < List.Count; i++)
            {
                Console.Write($"{List[i]} ");
            }
            Console.WriteLine("]");
        }

        enum ActionType
        {
            AddElement,
            PrintElements
        }
    }
}
