﻿/*
 * 1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.
 * Each Task should iterate from 1 to 1000 and print into the console the following string:
 * “Task #0 – {iteration number}”.
 */
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task1._100Tasks
{
    class Program
    {
        const int TaskAmount = 100;
        const int MaxIterationsCount = 1000;
        static int printsCount = 0;
        static object locker = new object();

        static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. Multi threading V1.");
            Console.WriteLine("1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.");
            Console.WriteLine("Each Task should iterate from 1 to 1000 and print into the console the following string:");
            Console.WriteLine("“Task #0 – {iteration number}”.");
            Console.WriteLine();

            HundredTasks();
            Console.ReadLine();
        }

        static void HundredTasks()
        {
            var tasks = new Task[TaskAmount];

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Factory.StartNew(IterationFunction, i);
            }

            Task.WaitAll(tasks);

            Console.WriteLine($"The count of prints {printsCount}");
        }

        static void IterationFunction(object taskNumberArg)
        {
            int? taskNumber = taskNumberArg as int?;
            if (taskNumber == null)
                return;
            IterateFromOneToMaxIterationsCount((int)taskNumber);
        }

        static void IterateFromOneToMaxIterationsCount(int taskNumber)
        {
            for (int i = 1; i <= MaxIterationsCount; i++)
            {
                Output(taskNumber, i);
            }
        }

        static void Output(int taskNumber, int iterationNumber)
        {
            Console.WriteLine($"Task #{taskNumber} – {iterationNumber}");

            # region This code isn't part of the task, but it's here to count prints in order to see if this function calls 100000 times. It is important because Console removes first printed items.
            lock (locker)
            {
                printsCount++;
            }
            #endregion
        }
    }
}
