﻿/*
*  Create a Task and attach continuations to it according to the following criteria:
   a.    Continuation task should be executed regardless of the result of the parent task.
   b.    Continuation task should be executed when the parent task finished without success.
   c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation
   d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled
   Demonstrate the work of the each case with console utility.
*/
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task6.Continuation
{
    class Program
    {
        static TaskCompletionSource<bool> TaskCompletionSource = new TaskCompletionSource<bool>();
        static void Main(string[] args)
        {
            Console.WriteLine("Create a Task and attach continuations to it according to the following criteria:");
            Console.WriteLine("a.    Continuation task should be executed regardless of the result of the parent task.");
            Console.WriteLine("b.    Continuation task should be executed when the parent task finished without success.");
            Console.WriteLine("c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation.");
            Console.WriteLine("d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled.");
            Console.WriteLine("Demonstrate the work of the each case with console utility.");
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Blue;
            GetChoiceFromConsole();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            //a.
            TaskCompletionSource.Task.ContinueWith((t1) => AlwaysExecuteFunction());

            //b.
            TaskCompletionSource.Task.ContinueWith((t1) => ExecuteWhenFaulted(), TaskContinuationOptions.OnlyOnFaulted);

            //c.
            TaskCompletionSource.Task.ContinueWith((t1) =>
            {
                if (t1.IsCanceled)
                {
                    ExecuteOnlyOnCanceled();
                }
            }, TaskContinuationOptions.ExecuteSynchronously);

            //d.
            TaskCompletionSource.Task.ContinueWith((t1) =>
            {
                if (t1.IsCanceled)
                {
                    ExecuteOnlyOnCanceled();
                    Console.WriteLine($"Is Thread Pool Thread: {Thread.CurrentThread.IsThreadPoolThread}");
                }
            }, TaskContinuationOptions.ExecuteSynchronously).ConfigureAwait(false);

            Console.ReadLine();
        }

        static void AlwaysExecuteFunction()
        {
            Console.WriteLine("Always Execute function run.");
        }

        static void ExecuteWhenFaulted()
        {
            Console.WriteLine("Execute When Faulted function run.");
        }

        static void ExecuteOnlyOnCanceled()
        {
            Console.WriteLine("Execute Only On Canceled function run.");
        }

        static void GetChoiceFromConsole()
        {
            bool choiceIsNotSelected = true;

            while (choiceIsNotSelected)
            {
                Console.WriteLine("Please select:");
                Console.WriteLine("1: set task as canceled");
                Console.WriteLine("2: set exception in task");
                Console.WriteLine("Put your choice here:");

                var choice = Console.ReadLine();

                int choiceInt;

                if (int.TryParse(choice, out choiceInt))
                {
                    switch (choiceInt)
                    {
                        case 1:
                            TaskCompletionSource.SetCanceled();
                            choiceIsNotSelected = false;
                            break;
                        case 2:
                            TaskCompletionSource.SetException(new Exception());
                            choiceIsNotSelected = false;
                            break;
                    }
                }
                if (choiceIsNotSelected)
                {
                    Console.WriteLine("incorrect choise please try again.");
                }

            }
        }
    }
}
