﻿/*
 * 2.	Write a program, which creates a chain of four Tasks.
 * First Task – creates an array of 10 random integer.
 * Second Task – multiplies this array with another random integer.
 * Third Task – sorts this array by ascending.
 * Fourth Task – calculates the average value. All this tasks should print the values to console.
 */
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MultiThreading.Task2.Chaining
{
    class Program
    {
        static readonly Random Random = new Random();
        const int ArrayLength = 10;
        const int RandomIntMaxValue = 100;
        static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. MultiThreading V1 ");
            Console.WriteLine("2.	Write a program, which creates a chain of four Tasks.");
            Console.WriteLine("First Task – creates an array of 10 random integer.");
            Console.WriteLine("Second Task – multiplies this array with another random integer.");
            Console.WriteLine("Third Task – sorts this array by ascending.");
            Console.WriteLine("Fourth Task – calculates the average value. All this tasks should print the values to console");
            Console.WriteLine();

            // feel free to add your code
            Task.Run(
                () => CreateArrayOfRandomInts(ArrayLength))
                .ContinueWith(t => MultiplieArrayWithRandomNumber(t.Result))
                .ContinueWith(t => SortArrayByAscending(t.Result))
                .ContinueWith(t => CalculateAverageValueOfArray(t.Result)
             );

            Console.ReadLine();
        }

        static int[] CreateArrayOfRandomInts(int length)
        {
            var array = new int[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = Random.Next(RandomIntMaxValue);
            }
            Console.Write("Array of random ints: ");
            PrintArrayValues(array);
            return array;
        }

        static int[] MultiplieArrayWithRandomNumber(int[] array)
        {
            var randomInt = Random.Next(RandomIntMaxValue);
            Console.WriteLine($"Random int to multiplie: {randomInt}");
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = array[i] * randomInt;
            }
            Console.Write("Multiplied array: ");
            PrintArrayValues(array);
            return array;
        }

        static int[] SortArrayByAscending(int[] array)
        {
            Array.Sort(array);
            Console.Write("Sorted array: ");
            PrintArrayValues(array);
            return array;
        }

        static void CalculateAverageValueOfArray(int[] array)
        {
            var average = array.Average();
            Console.WriteLine($"Average of array: {average.ToString()}");
        }
        static void PrintArrayValues(int[] array)
        {
            Console.Write('[');
            for (int i = 0; i < array.Length; i++)
            {
                var printString = i == array.Length - 1 ? $"{array[i]}" : $"{array[i]}, ";
                Console.Write(printString);
            }
            Console.WriteLine(']');
        }
    }
}
