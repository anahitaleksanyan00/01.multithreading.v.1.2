﻿/*
 * 4.	Write a program which recursively creates 10 threads.
 * Each thread should be with the same body and receive a state with integer number, decrement it,
 * print and pass as a state into the newly created thread.
 * Use Thread class for this task and Join for waiting threads.
 * 
 * Implement all of the following options:
 * - a) Use Thread class for this task and Join for waiting threads.
 * - b) ThreadPool class for this task and Semaphore for waiting threads.
 */

using System;
using System.Threading;

namespace MultiThreading.Task4.Threads.Join
{
    class Program
    {

        static Semaphore Semaphore = new Semaphore(1, 1);
        const int ThreadCount = 10;
        static void Main(string[] args)
        {
            Console.WriteLine("4.	Write a program which recursively creates 10 threads.");
            Console.WriteLine("Each thread should be with the same body and receive a state with integer number, decrement it, print and pass as a state into the newly created thread.");
            Console.WriteLine("Implement all of the following options:");
            Console.WriteLine();
            Console.WriteLine("- a) Use Thread class for this task and Join for waiting threads.");
            Console.WriteLine("- b) ThreadPool class for this task and Semaphore for waiting threads.");

            Console.WriteLine();

            // feel free to add your code
            CreteThread(ThreadCount);
            CreteThreadWithThreadPool(ThreadCount);

            Console.ReadLine();
        }

        static void CreteThread(int state)
        {
            if (state == 0) return;
            Thread t = new Thread(() => state = ThreadFunction(state));
            t.Start();
            t.Join();

            CreteThread(state);
        }

        static void CreteThreadWithThreadPool(int state)
        {
            //I couldn't find the best way to use Semaphore for this task. In my opinion it seems to work without Semaphore.
            Semaphore.WaitOne();
            if (state == 0) return;
            ThreadPool.QueueUserWorkItem<int>(
                (int stateParam) =>
                {
                    CreteThreadWithThreadPool(ThreadFunction(stateParam));
                }, state, true);
            Semaphore.Release();
        }

        static int ThreadFunction(int state)
        {
            Console.WriteLine(state--);
            return state;
        }
    }
}
